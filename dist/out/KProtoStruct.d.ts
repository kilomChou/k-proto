export declare const KProtoStructCount = 4;
declare const _default: {
    "1_1": (string | number)[][][];
    "1_2": (string | number)[][][];
    "1_3": any[];
    "2_1": (string | number)[][][];
};
export default _default;
