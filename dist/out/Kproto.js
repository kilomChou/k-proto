"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KProto = void 0;
/**
 * k-proto协议
 * (工具生成，请不要手动修改...)
 */
var KProto;
(function (KProto) {
    let b, Coder;
    /** 传入k-coder来进行初始化 */
    function init(kcoder) {
        const { KCoderVer, KBinary, StructCoder } = kcoder;
        if (!KCoderVer.checkVer("6.1.1")) {
            throw new Error("表格数据初始化失败, kcoder版本号不同 need:6.1.1 cur:" + KCoderVer.ver);
        }
        b = new KBinary();
        Coder = StructCoder;
    }
    KProto.init = init;
    const idBufDic = {}, tosCoderDic = {}, tocCoderDic = {};
    /** 加载单个协议结构 */
    function load(protoId, struct) {
        let st;
        tosCoderDic[protoId] = (st = struct[protoId][0]) ? new Coder(st) : undefined;
        tocCoderDic[protoId] = (st = struct[protoId][1]) ? new Coder(st) : undefined;
        if (!idBufDic[protoId]) {
            const id = protoId.split("_");
            idBufDic[protoId] = b.wStart().wAuint32(id[0]).wAuint32(id[1]).wEnd();
        }
    }
    KProto.load = load;
    /** 加载所有协议结构 */
    function loadAll(struct, onCount) {
        let c = 0;
        for (let protoId in struct) {
            load(protoId, struct);
            if (onCount) {
                onCount(++c);
            }
        }
    }
    KProto.loadAll = loadAll;
    /** 编码协议id */
    function encodeId(id) {
        b.wStart().wBuf(idBufDic[id]);
    }
    KProto.encodeId = encodeId;
    function encodeTos(id, data) {
        var _a;
        (_a = tosCoderDic[id]) === null || _a === void 0 ? void 0 : _a.encode(data, b);
        return b.wEnd();
    }
    KProto.encodeTos = encodeTos;
    function encodeToc(id, data) {
        var _a;
        (_a = tocCoderDic[id]) === null || _a === void 0 ? void 0 : _a.encode(data, b);
        return b.wEnd();
    }
    KProto.encodeToc = encodeToc;
    /** 解码协议id */
    function decodeId(buf) {
        b.rStart(buf);
        return `${b.rAuint32()}_${b.rAuint32()}`;
    }
    KProto.decodeId = decodeId;
    function decodeTos(id, buf) {
        var _a;
        return (_a = tosCoderDic[id]) === null || _a === void 0 ? void 0 : _a.decode(buf || b);
    }
    KProto.decodeTos = decodeTos;
    function decodeToc(id, buf) {
        var _a;
        return (_a = tocCoderDic[id]) === null || _a === void 0 ? void 0 : _a.decode(buf || b);
    }
    KProto.decodeToc = decodeToc;
    /** 解码协议id后剩下的数据buf */
    function getDataBuf() {
        return b.rCut(b.rBuf.length - b.rPos);
    }
    KProto.getDataBuf = getDataBuf;
    const win = globalThis || window;
    win["KProtoId"] = { test1: "1_1", test2: "1_2", test3: "1_3", test: "2_1" };
})(KProto || (exports.KProto = KProto = {}));
