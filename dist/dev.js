"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const KProtoBuilder_1 = require("./KProtoBuilder");
const protoDir = path_1.default.resolve(__dirname, "../proto");
const outDir = path_1.default.resolve(__dirname, "out");
new KProtoBuilder_1.KProtoBuilder(protoDir, outDir, false).build();
