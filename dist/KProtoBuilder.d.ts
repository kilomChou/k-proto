/**
 * 协议代码构建
 */
export declare class KProtoBuilder {
    /** 协议目录 */
    readonly inputDir: string;
    /** 输出目录 */
    readonly outputDir: string;
    /** 是否输出路由配置 */
    readonly outProtoCfg: boolean;
    /** 输出代码文件名 */
    readonly protoCodeName: string;
    /** 输出协议结构代码文件名 */
    readonly structCodeName: string;
    /** 输出协议路由代码文件名 */
    readonly routeCodeName: string;
    /** 输出协议id名 */
    readonly protoIdName: string;
    /** 输出协议定义前缀 */
    readonly prefix: string;
    /** 协议id字典 */
    private protoIdDic;
    /** 所有协议结构定义 */
    private allProtoDefs;
    /** 协议定义 */
    private protos;
    constructor(
    /** 协议目录 */
    inputDir: string, 
    /** 输出目录 */
    outputDir: string, 
    /** 是否输出路由配置 */
    outProtoCfg: boolean, 
    /** 输出代码文件名 */
    protoCodeName?: string, 
    /** 输出协议结构代码文件名 */
    structCodeName?: string, 
    /** 输出协议路由代码文件名 */
    routeCodeName?: string, 
    /** 输出协议id名 */
    protoIdName?: string, 
    /** 输出协议定义前缀 */
    prefix?: string);
    /** 构建协议  */
    build(onDone?: (time: number, fileCount: number, protoCount: number, typeCount: number) => void): void;
    /** 读取所有协议 */
    private loadAllProto;
    /** 读取单个协议 */
    private loadProto;
    /** 排序 */
    private sort;
    /** 构建协议代码 */
    private buildProtoCode;
    buildStructCode(): void;
    buildRouteCode(): void;
}
