"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KProtoBuilder = void 0;
const fs_1 = __importDefault(require("fs"));
const k_coder_1 = require("k-coder");
const path_1 = __importDefault(require("path"));
/**
 * 协议代码构建
 */
class KProtoBuilder {
    constructor(
    /** 协议目录 */
    inputDir, 
    /** 输出目录 */
    outputDir, 
    /** 是否输出路由配置 */
    outProtoCfg, 
    /** 输出代码文件名 */
    protoCodeName = "KProto", 
    /** 输出协议结构代码文件名 */
    structCodeName = "KProtoStruct", 
    /** 输出协议路由代码文件名 */
    routeCodeName = "KProtoCfg", 
    /** 输出协议id名 */
    protoIdName = "KProtoId", 
    /** 输出协议定义前缀 */
    prefix = "kp") {
        this.inputDir = inputDir;
        this.outputDir = outputDir;
        this.outProtoCfg = outProtoCfg;
        this.protoCodeName = protoCodeName;
        this.structCodeName = structCodeName;
        this.routeCodeName = routeCodeName;
        this.protoIdName = protoIdName;
        this.prefix = prefix;
        /** 协议id字典 */
        this.protoIdDic = {};
        /** 所有协议结构定义 */
        this.allProtoDefs = [];
        /** 协议定义 */
        this.protos = [];
    }
    /** 构建协议  */
    build(onDone) {
        const now = Date.now();
        this.loadAllProto(() => {
            this.sort();
            this.buildProtoCode();
            this.buildStructCode();
            this.outProtoCfg && this.buildRouteCode();
            if (onDone) {
                let protoCount = 0;
                let typeCount = 0;
                this.protos.forEach(proto => {
                    protoCount += proto.protoDefs.length;
                    typeCount += proto.protoDefs.length;
                });
                onDone(Date.now() - now, this.protos.length, protoCount, typeCount);
            }
        });
    }
    /** 读取所有协议 */
    loadAllProto(onDone) {
        //遍历协议目录读取协议
        console.log("===> 读取协议");
        const now = Date.now();
        const files = fs_1.default.readdirSync(this.inputDir);
        let count = files.length;
        files.forEach(fileName => {
            const filePath = path_1.default.resolve(this.inputDir, fileName);
            const match = fileName.match(/(.*)\.kproto$/);
            if (match) {
                console.log(filePath);
                //检查命名是否规范
                assert(!match[1].match(/[^_0-9a-zA-Z]/), `[${fileName}.kproto]命名不规范`);
                fs_1.default.readFile(filePath, "utf-8", (err, data) => {
                    if (err) {
                        console.log(`${fileName}读取失败！`);
                        fileDone();
                        return;
                    }
                    this.loadProto(data, match[1]);
                    fileDone();
                });
            }
        });
        function fileDone() {
            if (--count === 0) {
                console.log(`耗时：${Date.now() - now}ms`);
                onDone();
            }
        }
    }
    /** 读取单个协议 */
    loadProto(data, fileName) {
        const that = this;
        const typeDefs = [];
        const protoDefs = [];
        this.protos.push({ fileName, typeDefs, protoDefs });
        //忽略#号后内容
        data = data.replace(/#.*/g, "");
        let def;
        let typeDef;
        let protoDef;
        let note = [];
        let fieldIdx = 0;
        const nameDic = {};
        //遍历每行读取定义
        data.split(/\r?\n/).forEach((ln, lnIdx) => {
            if (!ln)
                return;
            let match = ln.match(/^\s*\/\/(.*)/);
            if (match) {
                //匹配到注释
                note.push(match[1]);
                return;
            }
            if (!typeDef && !protoDef) {
                //===匹配定义
                match = ln.match(/^\s*(type|\d+_\d+(?::[_0-9a-zA-Z\.]+)?) +([_0-9a-zA-Z]+) *{/);
                if (match) {
                    //匹配到定义
                    const name = match[2];
                    assert(!nameDic[name], errMsg("名称定义重复"));
                    nameDic[name] = true;
                    if (match[1] === "type") {
                        assert(match[3] != ";", errMsg("类型结构定义不能为空"));
                        //通用结构定义
                        fieldIdx = 0;
                        typeDefs.push(def = typeDef = {
                            fileName,
                            name: name,
                            note,
                            fields: [],
                            struct: "[]",
                        });
                    }
                    else {
                        //协议结构定义
                        const [protoId, router] = match[1].split(":");
                        assert(!this.protoIdDic[protoId], errMsg("协议号重复"));
                        protoDef = {
                            protoId,
                            name,
                            fileName,
                            router: router ? `${router}.${name}` : "",
                            note
                        };
                        this.protoIdDic[protoId] = true;
                        this.allProtoDefs.push(protoDef);
                        protoDefs.push(protoDef);
                    }
                }
                else {
                    checkOther(ln);
                }
            }
            else {
                //===读取定义
                /** 匹配字段 */
                function matchField(fields) {
                    match = ln.match(fieldReg);
                    if (match) {
                        //匹配到字段
                        fields.push(parseField(match));
                        return true;
                    }
                    return false;
                }
                /** 匹配结束 */
                function matchEnd() {
                    if (ln.match(/^\s*\}/)) {
                        return true;
                    }
                    else {
                        checkOther(ln);
                    }
                    return false;
                }
                if (typeDef) {
                    //===读取类型结构定义
                    if (!matchField(typeDef.fields)) {
                        if (matchEnd()) {
                            typeDef.struct = buildFieldsStruct(typeDef.fields);
                            assert(typeDef.struct, "类型结构定义不能为空");
                            def = typeDef = null;
                        }
                    }
                }
                else if (protoDef) {
                    //===读取协议结构定义
                    if (!def) {
                        //匹配tos/toc结构
                        match = ln.match(/^\s*(tos|toc) *({|;)/);
                        if (match) {
                            fieldIdx = 0;
                            def = {
                                note: note.length > 0 ? note : protoDef.note,
                                name: `${protoDef.name}_${match[1]}`,
                                fields: [],
                                struct: ""
                            };
                            if (match[1] === "tos") {
                                protoDef.tosStruct = def;
                            }
                            else {
                                protoDef.tocStruct = def;
                            }
                            if (match[2] === ";") {
                                //没有结构
                                def = null;
                            }
                        }
                        else if (matchEnd()) {
                            protoDef = null;
                        }
                    }
                    else {
                        //===读取tos/toc结构定义
                        if (!matchField(def.fields)) {
                            if (matchEnd()) {
                                def.struct = buildFieldsStruct(def.fields);
                                def = null;
                            }
                        }
                    }
                }
            }
            note = [];
            function errMsg(msg) {
                return `(行:${lnIdx + 1})${ln} >>> ${msg}`;
            }
            function checkOther(ln) {
                assert(!/\S/.test(ln), errMsg("错误定义"));
            }
            /** 解析字段 */
            function parseField(match) {
                const fieldName = match[1];
                /** 是否使用类型结构 */
                const isType = match[2] === "@";
                let type = match[3];
                let svt = k_coder_1.StructValueType[type];
                switch (type) {
                    case "uint":
                        svt = k_coder_1.StructValueType.auint32;
                        break;
                    case "int":
                        svt = k_coder_1.StructValueType.aint32;
                        break;
                    case "floatUint":
                        svt = k_coder_1.StructValueType.floatAuint32;
                        break;
                    case "floatInt":
                        svt = k_coder_1.StructValueType.floatAint32;
                        break;
                }
                /** 特殊浮点型参数 */
                const floatParam = match[4] ? parseInt(match[4]) : 0;
                /** 数组维度 */
                const arrStr = match[5] || "";
                const arrVec = Math.floor(arrStr.length / 2);
                let comSt = "";
                let tsType;
                const struct = [`"${fieldName}"`];
                switch (svt) {
                    case k_coder_1.StructValueType.bool:
                        tsType = "boolean";
                        struct.push(svt);
                        break;
                    case k_coder_1.StructValueType.byte:
                    case k_coder_1.StructValueType.uint8:
                    case k_coder_1.StructValueType.uint16:
                    case k_coder_1.StructValueType.uint32:
                    case k_coder_1.StructValueType.uint64:
                    case k_coder_1.StructValueType.auint32:
                    case k_coder_1.StructValueType.auint64:
                    case k_coder_1.StructValueType.int8:
                    case k_coder_1.StructValueType.int16:
                    case k_coder_1.StructValueType.int32:
                    case k_coder_1.StructValueType.int64:
                    case k_coder_1.StructValueType.aint32:
                    case k_coder_1.StructValueType.aint64:
                    case k_coder_1.StructValueType.float32:
                    case k_coder_1.StructValueType.floatAuint32:
                    case k_coder_1.StructValueType.floatAuint64:
                    case k_coder_1.StructValueType.floatAint32:
                    case k_coder_1.StructValueType.floatAint64:
                    case k_coder_1.StructValueType.floatStr:
                        tsType = "number";
                        struct.push(svt);
                        break;
                    case k_coder_1.StructValueType.bytes:
                        tsType = "number[]";
                        struct.push(svt);
                        break;
                    case k_coder_1.StructValueType.str:
                        tsType = "string";
                        struct.push(svt);
                        break;
                    case k_coder_1.StructValueType.any:
                        tsType = "any";
                        struct.push(svt);
                        break;
                    case k_coder_1.StructValueType.obj:
                        tsType = "{ [key: string]: any }";
                        struct.push(svt);
                        break;
                    case k_coder_1.StructValueType.arr:
                        tsType = "any[]";
                        struct.push(svt);
                        break;
                    default:
                        if (isType) {
                            //类型结构
                            const split = type.split(".");
                            tsType = `${that.prefix}_${split.length > 1 ? `${split[0]}_${split[1]}` : `${fileName}_${type}`}`;
                            type = split.length > 1 ? type : `${fileName}.${type}`;
                            if (typeDef) {
                                typeDef.deps || (typeDef.deps = []);
                                typeDef.deps.push(fieldIdx, `"${type}"`);
                            }
                            else {
                                comSt = `_["${type}"]`;
                            }
                            struct.push(k_coder_1.StructValueType.struct);
                        }
                        else {
                            assert(false, errMsg("字段解析失败"));
                        }
                        break;
                }
                //数组维度
                tsType += arrStr;
                struct.push(arrVec);
                //特殊参数
                floatParam && struct.push(floatParam);
                comSt && struct.push(comSt);
                !arrVec && struct.length === 3 && struct.pop();
                ++fieldIdx;
                return {
                    lnIdx,
                    note,
                    name: fieldName,
                    tsType,
                    struct: `[${struct.join(", ")}]`,
                };
            }
        });
        /** 构建字段结构 */
        function buildFieldsStruct(fields) {
            if (fields.length) {
                let structs = [];
                fields.forEach(field => structs.push(field.struct));
                return `[${structs.join(", ")}]`;
            }
            else {
                return "";
            }
        }
    }
    /** 排序 */
    sort() {
        this.allProtoDefs.sort((a, b) => a.protoId.localeCompare(b.protoId));
        this.protos.sort((a, b) => a.fileName > b.fileName ? 1 : a.fileName < b.fileName ? -1 : 0);
    }
    /** 构建协议代码 */
    buildProtoCode() {
        console.log("===> 构建协议代码");
        const now = Date.now();
        let code = "";
        code += `/**\n`;
        code += ` * k-proto协议\n`;
        code += ` * (工具生成，请不要手动修改...)\n`;
        code += ` */\n`;
        code += `export namespace ${this.protoCodeName} {\n`;
        code += `\n`;
        code += `    let b: any, Coder: any;\n`;
        code += `    /** 传入k-coder来进行初始化 */\n`;
        code += `    export function init(kcoder: { KCoderVer: any, KBinary: any, StructCoder: any }) {\n`;
        code += `        const { KCoderVer, KBinary, StructCoder } = kcoder;\n`;
        code += `        if (!KCoderVer.checkVer("${k_coder_1.KCoderVer.ver}")) {\n`;
        code += `            throw new Error("表格数据初始化失败, kcoder版本号不同 need:${k_coder_1.KCoderVer.ver} cur:" + KCoderVer.ver);\n`;
        code += `        }\n`;
        code += `        b = new KBinary();\n`;
        code += `        Coder = StructCoder;\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    const idBufDic: any = {}, tosCoderDic: any = {}, tocCoderDic: any = {};\n`;
        code += `\n`;
        code += `    /** 加载单个协议结构 */\n`;
        code += `    export function load(protoId: any, struct: any) {\n`;
        code += `        let st: any;\n`;
        code += `        tosCoderDic[protoId] = (st = struct[protoId][0]) ? new Coder(st) : undefined;\n`;
        code += `        tocCoderDic[protoId] = (st = struct[protoId][1]) ? new Coder(st) : undefined;\n`;
        code += `        if (!idBufDic[protoId]) {\n`;
        code += `            const id = protoId.split("_");\n`;
        code += `            idBufDic[protoId] = b.wStart().wAuint32(id[0]).wAuint32(id[1]).wEnd();\n`;
        code += `        }\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 加载所有协议结构 */\n`;
        code += `    export function loadAll(struct: any, onCount?: (count: number) => void) {\n`;
        code += `        let c = 0;\n`;
        code += `        for (let protoId in struct) {\n`;
        code += `            load(protoId, struct);\n`;
        code += `            if (onCount) {\n`;
        code += `                onCount(++c);\n`;
        code += `            }\n`;
        code += `        }\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 编码协议id */\n`;
        code += `    export function encodeId(id: ${this.protoIdName}) {\n`;
        code += `        b.wStart().wBuf(idBufDic[id]);\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 编码tos协议 */\n`;
        code += `    export function encodeTos<ID extends ${this.protoCodeName}TosNeed>(id: ID, data: ${this.protoCodeName}Tos[ID]): Uint8Array\n`;
        code += `    export function encodeTos<ID extends ${this.protoCodeName}TosNever>(id: ID): Uint8Array\n`;
        code += `    export function encodeTos(id: ${this.protoIdName}, data?: any): Uint8Array {\n`;
        code += `        tosCoderDic[id]?.encode(data, b);\n`;
        code += `        return b.wEnd();\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 编码toc协议 */\n`;
        code += `    export function encodeToc<ID extends ${this.protoCodeName}TocNeed>(id: ID, data: ${this.protoCodeName}Toc[ID]): Uint8Array\n`;
        code += `    export function encodeToc<ID extends ${this.protoCodeName}TocNever>(id: ID): Uint8Array\n`;
        code += `    export function encodeToc(id: ${this.protoIdName}, data?: any): Uint8Array {\n`;
        code += `        tocCoderDic[id]?.encode(data, b);\n`;
        code += `        return b.wEnd();\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 解码协议id */\n`;
        code += `    export function decodeId(buf: Uint8Array) {\n`;
        code += `        b.rStart(buf);\n`;
        code += `        return \`\${b.rAuint32()}_\${b.rAuint32()}\`;\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 解码tos协议 */\n`;
        code += `    export function decodeTos<ID extends ${this.protoCodeName}TosNeed>(id: ID, buf?: Uint8Array): ${this.protoCodeName}Tos[ID]\n`;
        code += `    export function decodeTos<ID extends ${this.protoCodeName}TosNever>(id: ID, buf?: Uint8Array): void\n`;
        code += `    export function decodeTos(id: ${this.protoIdName}, buf?: Uint8Array) {\n`;
        code += `        return tosCoderDic[id]?.decode(buf || b);\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 解码toc协议 */\n`;
        code += `    export function decodeToc<ID extends ${this.protoCodeName}TocNeed>(id: ID, buf?: Uint8Array): ${this.protoCodeName}Toc[ID]\n`;
        code += `    export function decodeToc<ID extends ${this.protoCodeName}TocNever>(id: ID, buf?: Uint8Array): void\n`;
        code += `    export function decodeToc(id: ${this.protoIdName}, buf?: Uint8Array) {\n`;
        code += `        return tocCoderDic[id]?.decode(buf || b);\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    /** 解码协议id后剩下的数据buf */\n`;
        code += `    export function getDataBuf() {\n`;
        code += `        return b.rCut(b.rBuf.length - b.rPos);\n`;
        code += `    }\n`;
        code += `\n`;
        code += `    const win: any = globalThis || window;\n`;
        code += `    win["${this.protoIdName}"] = { ${this.allProtoDefs.map(def => `${def.name}: "${def.protoId}"`).join(", ")} };\n`;
        code += `\n`;
        code += `}\n`;
        code += `\n`;
        code += `declare global {\n`;
        code += `\n`;
        code += "    /** 协议号 */\n";
        code += `    enum ${this.protoIdName} {\n`;
        for (const def of this.allProtoDefs) {
            buildNote(def.note, "        ");
            code += `        ${def.name} = "${def.protoId}",\n`;
        }
        code += "    }\n";
        code += "\n";
        code += `    /** 发往服务端的协议 */\n`;
        code += `    interface ${this.protoCodeName}Tos {\n`;
        for (const def of this.allProtoDefs) {
            if (!def.tosStruct)
                continue;
            code += `        [${this.protoIdName}.${def.name}]: ${def.tosStruct.fields.length > 0 ? `${this.prefix}_${def.fileName}_${def.name}_tos` : "never"}\n`;
        }
        code += `    }\n`;
        code += `\n`;
        code += `    type ${this.protoCodeName}TosNeed = keyof Pick<${this.protoCodeName}Tos, { [key in keyof ${this.protoCodeName}Tos]: ${this.protoCodeName}Tos[key] extends never ? never : key }[keyof ${this.protoCodeName}Tos]>;\n`;
        code += `    type ${this.protoCodeName}TosNever = Exclude<keyof ${this.protoCodeName}Tos, ${this.protoCodeName}TosNeed>;\n`;
        code += `\n`;
        code += `    /** 发往客户端的协议 */\n`;
        code += `    interface ${this.protoCodeName}Toc {\n`;
        for (const def of this.allProtoDefs) {
            if (!def.tocStruct || !def.tocStruct.fields.length)
                continue;
            code += `        [${this.protoIdName}.${def.name}]: ${this.prefix}_${def.fileName}_${def.name}_toc\n`;
        }
        code += `    }\n`;
        code += `\n`;
        code += `    type ${this.protoCodeName}TocNeed = keyof Pick<${this.protoCodeName}Toc, { [key in keyof ${this.protoCodeName}Toc]: ${this.protoCodeName}Toc[key] extends never ? never : key }[keyof ${this.protoCodeName}Toc]>;\n`;
        code += `    type ${this.protoCodeName}TocNever = Exclude<keyof ${this.protoCodeName}Toc, ${this.protoCodeName}TocNeed>;\n`;
        code += `\n`;
        //写入协议结构
        for (const proto of this.protos) {
            const defs = [];
            for (const def of proto.typeDefs) {
                defs.push(def);
            }
            for (const def of proto.protoDefs) {
                def.tocStruct && def.tocStruct.fields.length && defs.push(def.tocStruct);
                def.tosStruct && def.tosStruct.fields.length && defs.push(def.tosStruct);
            }
            for (const def of defs) {
                buildNote(def.note, "    ");
                code += `    interface ${this.prefix}_${proto.fileName}_${def.name} {\n`;
                def.fields.forEach(field => {
                    buildNote(field.note, "        ");
                    code += `        ${field.name}: ${field.tsType},\n`;
                });
                code += `    }\n\n`;
            }
        }
        code += `}`;
        fs_1.default.writeFileSync(path_1.default.resolve(this.outputDir, `${this.protoCodeName}.ts`), code, { encoding: "utf-8", flag: "w+" });
        /** 构建注释 tab：缩进*/
        function buildNote(note, tab) {
            if (!note || !note.length)
                return;
            if (note.length === 1) {
                //单行注释
                code += `${tab}/** ${note} */\n`;
            }
            else {
                //多行注释
                code += `${tab}/**\n`;
                note.forEach(line => code += `${tab} * ${line}\n`);
                code += `${tab} */\n`;
            }
        }
        console.log(`耗时：${Date.now() - now}ms`);
    }
    buildStructCode() {
        console.log("===> 构建结构代码");
        const now = Date.now();
        let code = "//工具生成，请不要手动修改...\n";
        //类型结构
        code += `function $(key: string, st: any, ...deps: any[]) {\n`;
        code += `    deps.length && depFns.push(() => {\n`;
        code += `        for (let i = 0; i < deps.length; ++i) {\n`;
        code += `            _[key][deps[i]][3] = _[deps[++i]];\n`;
        code += `        }\n`;
        code += `    });\n`;
        code += `    Object.defineProperty(_, key, { get: () => Object.defineProperty(_, key, { value: st })[key], enumerable: true, configurable: true })[key];\n`;
        code += `}\n`;
        code += `const _: any = {}, depFns: (() => void)[] = [];\n`;
        for (const proto of this.protos) {
            for (const def of proto.typeDefs) {
                code += `$("${proto.fileName}.${def.name}", ${def.struct}${def.deps ? `, ${def.deps.join(", ")}` : ""});\n`;
            }
        }
        code += `depFns.forEach(fn => fn());\n\n`;
        code += `export const ${this.structCodeName}Count = ${this.allProtoDefs.length};\n`;
        code += `export default {\n`;
        for (const def of this.allProtoDefs) {
            code += `    "${def.protoId}": [${def.tosStruct ? def.tosStruct.struct : ""},${def.tocStruct ? ` ${def.tocStruct.struct}` : ""}],\n`;
        }
        code += `}`;
        fs_1.default.writeFileSync(path_1.default.resolve(this.outputDir, `${this.structCodeName}.ts`), code, { encoding: "utf-8", flag: "w+" });
        console.log(`耗时：${Date.now() - now}ms`);
    }
    buildRouteCode() {
        console.log("===> 构建结构代码");
        const now = Date.now();
        let code = "//工具生成，请不要手动修改...\n";
        code += `export default { \n`;
        for (let def of this.allProtoDefs) {
            if (!def.router)
                continue;
            code += `    "${def.protoId}": "${def.router}", \n`;
        }
        code += `}`;
        fs_1.default.writeFileSync(path_1.default.resolve(this.outputDir, `${this.routeCodeName}.ts`), code, { encoding: "utf-8", flag: "w+" });
        console.log(`耗时：${Date.now() - now}ms`);
    }
}
exports.KProtoBuilder = KProtoBuilder;
/** 字段正则 */
const fieldReg = /^\s*([_0-9a-zA-Z]+\??) *: *(?:(@?)([\._0-9a-zA-Z]+)(?:\((\d+)\))*((?:\[\])+)*)+ *;?/;
/** 断言失败退出程序并输出错误信息 */
function assert(cond, msg) {
    if (!cond) {
        console.log(msg);
        process.exit();
    }
    return cond;
}
