import path from "path";
import { KProtoBuilder } from "./KProtoBuilder";

const protoDir = path.resolve(__dirname, "../proto");
const outDir = path.resolve(__dirname, "out");

new KProtoBuilder(protoDir, outDir, false).build();