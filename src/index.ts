#!/usr/bin/env node
import path from "path";
import { program } from "commander";
import { KProtoBuilder } from "./KProtoBuilder";

program.requiredOption("-i <协议目录>", "协议目录")
    .requiredOption("-o <代码导出目录>", "代码导出目录")
    .option("-cfg", "是否导出路由配置")
    .option("-cn <导出代码文件名>", "导出代码文件名(不用加后缀)")
    .option("-scn <导出结构代码文件名>", "导出结构代码文件名(不用加后缀)")
    .option("-ccn <导出路由代码文件名>", "导出路由代码文件名(不用加后缀)")
    .option("-idn <导出协议id名>", "导出协议id名")
    .option("-p <导出定义代码前缀>", "导出定义代码前缀")
    .parse(process.argv);

let { i: inputDir, o: outputDir, Cfg: outCfg, Cn: codeName, Scn: structCodeName, Ccn: cfgCodeName, Idn: idName, p: prefix } = program.opts();

inputDir = path.resolve(inputDir);
outputDir = path.resolve(outputDir);
console.log(`[协议文件目录]：${inputDir}`);
console.log(`[代码导出目录]：${outputDir}`);
console.log("========= 开始构建协议 =========");
console.log("............................");
new KProtoBuilder(inputDir, outputDir, outCfg, codeName, structCodeName, cfgCodeName, idName, prefix).build((time, fileCount, protoCount, typeCount) => {
    console.log("............................");
    console.log("========= 构建完成 =========");
    console.log(`文件数量：${fileCount}`);
    console.log(`协议数量：${protoCount}`);
    console.log(`类型数量：${typeCount}`);
    console.log(`总耗时：${time / 1000}s`);
});
