/**
 * k-proto协议
 * (工具生成，请不要手动修改...)
 */
export namespace KProto {

    let b: any, Coder: any;
    /** 传入k-coder来进行初始化 */
    export function init(kcoder: { KCoderVer: any, KBinary: any, StructCoder: any }) {
        const { KCoderVer, KBinary, StructCoder } = kcoder;
        if (!KCoderVer.checkVer("6.1.1")) {
            throw new Error("表格数据初始化失败, kcoder版本号不同 need:6.1.1 cur:" + KCoderVer.ver);
        }
        b = new KBinary();
        Coder = StructCoder;
    }

    const idBufDic: any = {}, tosCoderDic: any = {}, tocCoderDic: any = {};

    /** 加载单个协议结构 */
    export function load(protoId: any, struct: any) {
        let st: any;
        tosCoderDic[protoId] = (st = struct[protoId][0]) ? new Coder(st) : undefined;
        tocCoderDic[protoId] = (st = struct[protoId][1]) ? new Coder(st) : undefined;
        if (!idBufDic[protoId]) {
            const id = protoId.split("_");
            idBufDic[protoId] = b.wStart().wAuint32(id[0]).wAuint32(id[1]).wEnd();
        }
    }

    /** 加载所有协议结构 */
    export function loadAll(struct: any, onCount?: (count: number) => void) {
        let c = 0;
        for (let protoId in struct) {
            load(protoId, struct);
            if (onCount) {
                onCount(++c);
            }
        }
    }

    /** 编码协议id */
    export function encodeId(id: KProtoId) {
        b.wStart().wBuf(idBufDic[id]);
    }

    /** 编码tos协议 */
    export function encodeTos<ID extends KProtoTosNeed>(id: ID, data: KProtoTos[ID]): Uint8Array
    export function encodeTos<ID extends KProtoTosNever>(id: ID): Uint8Array
    export function encodeTos(id: KProtoId, data?: any): Uint8Array {
        tosCoderDic[id]?.encode(data, b);
        return b.wEnd();
    }

    /** 编码toc协议 */
    export function encodeToc<ID extends KProtoTocNeed>(id: ID, data: KProtoToc[ID]): Uint8Array
    export function encodeToc<ID extends KProtoTocNever>(id: ID): Uint8Array
    export function encodeToc(id: KProtoId, data?: any): Uint8Array {
        tocCoderDic[id]?.encode(data, b);
        return b.wEnd();
    }

    /** 解码协议id */
    export function decodeId(buf: Uint8Array) {
        b.rStart(buf);
        return `${b.rAuint32()}_${b.rAuint32()}`;
    }

    /** 解码tos协议 */
    export function decodeTos<ID extends KProtoTosNeed>(id: ID, buf?: Uint8Array): KProtoTos[ID]
    export function decodeTos<ID extends KProtoTosNever>(id: ID, buf?: Uint8Array): void
    export function decodeTos(id: KProtoId, buf?: Uint8Array) {
        return tosCoderDic[id]?.decode(buf || b);
    }

    /** 解码toc协议 */
    export function decodeToc<ID extends KProtoTocNeed>(id: ID, buf?: Uint8Array): KProtoToc[ID]
    export function decodeToc<ID extends KProtoTocNever>(id: ID, buf?: Uint8Array): void
    export function decodeToc(id: KProtoId, buf?: Uint8Array) {
        return tocCoderDic[id]?.decode(buf || b);
    }

    /** 解码协议id后剩下的数据buf */
    export function getDataBuf() {
        return b.rCut(b.rBuf.length - b.rPos);
    }

    const win: any = globalThis || window;
    win["KProtoId"] = { test1: "1_1", test2: "1_2", test3: "1_3", test: "2_1" };

}

declare global {

    /** 协议号 */
    enum KProtoId {
        /** 协议注释 */
        test1 = "1_1",
        /** 协议注释 (如果tos或toc没有单独的注释则会取协议注释作为注释) */
        test2 = "1_2",
        /** 协议注释3，可以为空结构，发送数据时只会发送协议号 */
        test3 = "1_3",
        test = "2_1",
    }

    /** 发往服务端的协议 */
    interface KProtoTos {
        [KProtoId.test1]: kp_template_test1_tos
        [KProtoId.test2]: kp_template_test2_tos
        [KProtoId.test3]: never
        [KProtoId.test]: kp_template2_test_tos
    }

    type KProtoTosNeed = keyof Pick<KProtoTos, { [key in keyof KProtoTos]: KProtoTos[key] extends never ? never : key }[keyof KProtoTos]>;
    type KProtoTosNever = Exclude<keyof KProtoTos, KProtoTosNeed>;

    /** 发往客户端的协议 */
    interface KProtoToc {
        [KProtoId.test1]: kp_template_test1_toc
        [KProtoId.test2]: kp_template_test2_toc
        [KProtoId.test]: kp_template2_test_toc
    }

    type KProtoTocNeed = keyof Pick<KProtoToc, { [key in keyof KProtoToc]: KProtoToc[key] extends never ? never : key }[keyof KProtoToc]>;
    type KProtoTocNever = Exclude<keyof KProtoToc, KProtoTocNeed>;

    /** 类型注释 */
    interface kp_template_test_type1 {
        /** 布尔值 */
        b: boolean,
    }

    /** 类型注释2 */
    interface kp_template_test_type2 {
        /** 加@号引用自定义类型 */
        val: kp_template_test_type1,
        /** 自定义类型也支持数组 */
        arr: kp_template_test_type1[],
        /** 引用其他协议的自定义类型 */
        other: kp_template2_test_type,
    }

    /** 发客户端协议 */
    interface kp_template_test1_toc {
        /** 8位正整型 */
        num1: number,
        /** 16位正整型 */
        num2: number,
        /** 32位正整型 */
        num3: number,
        /** 64位正整型 */
        num4: number,
        /** 自适应32位正整型 */
        num5: number,
        /** 自适应64位正整型 */
        num6: number,
        /** 8位整型 */
        num7: number,
        /** 16位整型 */
        num8: number,
        /** 32位整型 */
        num9: number,
        /** 64位整型 */
        num10: number,
        /** 自适应32位整型 */
        num11: number,
        /** 自适应64位整型 */
        num12: number,
        /** 32位正浮点型 编码时会转化为自适应32位正整型 (参数保留n位小数) */
        num13: number,
        /** 64位正浮点型 编码时会转化为自适应64位正整型 (参数保留n位小数) */
        num14: number,
        /** 32位浮点型 编码时会转化为自适应32位整型 (参数保留n位小数) */
        num15: number,
        /** 64位浮点型 编码时会转化为自适应64位整型 (参数保留n位小数) */
        num16: number,
        /** 浮点型 编码时会转化为字符串 */
        num17: number,
    }

    /** 发服务端协议 */
    interface kp_template_test1_tos {
        /** 布尔值 */
        b: boolean,
        /** 自适应32位正整型(简写:同auint32) */
        num1: number,
        /** 自适应32位整型(简写:同aint32) */
        num2: number,
        /** 32位正浮点型 编码时会转化为自适应32位正整型(简写:同floatAuint32) */
        num3: number,
        /** 32位浮点型 自适应32位整型(简写:同floatAint32) */
        num4: number,
        /** 字符串 */
        str: string,
        /** any类型 */
        any: any,
        /** 对象类型 */
        obj: { [key: string]: any },
        /** any数组类型 */
        arr: any[],
    }

    /** 协议注释 (如果tos或toc没有单独的注释则会取协议注释作为注释) */
    interface kp_template_test2_toc {
        /** 布尔值二维数组 */
        b: boolean[][],
        /** 自适应32位正整型二维数组 */
        num1: number[][],
        /** 自适应32位整型二维数组 */
        num2: number[][],
        /** 32位正浮点型 编码时会转化为自适应32位正整型二维数组 */
        num3: number[][],
        /** 32位浮点型 自适应32位整型二维数组 */
        num4: number[][],
        /** 字符串二维数组 */
        str: string[][],
    }

    /** 协议注释 (如果tos或toc没有单独的注释则会取协议注释作为注释) */
    interface kp_template_test2_tos {
        /** 布尔值数组 */
        b: boolean[],
        /** 自适应32位正整型数组 */
        num1: number[],
        /** 自适应32位整型数组 */
        num2: number[],
        /** 32位正浮点型 编码时会转化为自适应32位正整型数组 */
        num3: number[],
        /** 32位浮点型 自适应32位整型数组 */
        num4: number[],
        /** 字符串数组 */
        str: string[],
    }

    interface kp_template2_test_type {
        /** 布尔值 */
        b: boolean,
    }

    interface kp_template2_test_toc {
        /** 布尔值 */
        b: boolean,
    }

    interface kp_template2_test_tos {
        /** 布尔值 */
        b: boolean,
    }

}