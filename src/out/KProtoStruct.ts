//工具生成，请不要手动修改...
function $(key: string, st: any, ...deps: any[]) {
    deps.length && depFns.push(() => {
        for (let i = 0; i < deps.length; ++i) {
            _[key][deps[i]][3] = _[deps[++i]];
        }
    });
    Object.defineProperty(_, key, { get: () => Object.defineProperty(_, key, { value: st })[key], enumerable: true, configurable: true })[key];
}
const _: any = {}, depFns: (() => void)[] = [];
$("template.test_type1", [["b", 0]]);
$("template.test_type2", [["val", 23], ["arr", 23, 1], ["other", 23]], 0, "template.test_type1", 1, "template.test_type1", 2, "template2.test_type");
$("template2.test_type", [["b", 0]]);
depFns.forEach(fn => fn());

export const KProtoStructCount = 4;
export default {
    "1_1": [[["b", 0], ["num1", 7], ["num2", 13], ["num3", 17, 0, 2], ["num4", 19, 0, 2], ["str", 22], ["any", 24], ["obj", 25], ["arr", 26]], [["num1", 3], ["num2", 4], ["num3", 5], ["num4", 6], ["num5", 7], ["num6", 8], ["num7", 9], ["num8", 10], ["num9", 11], ["num10", 12], ["num11", 13], ["num12", 14], ["num13", 17, 0, 2], ["num14", 18, 0, 2], ["num15", 19, 0, 2], ["num16", 20, 0, 2], ["num17", 21]]],
    "1_2": [[["b", 0, 1], ["num1", 7, 1], ["num2", 13, 1], ["num3", 17, 1, 2], ["num4", 19, 1, 2], ["str", 22, 1]], [["b", 0, 2], ["num1", 7, 2], ["num2", 13, 2], ["num3", 17, 2, 2], ["num4", 19, 2, 2], ["str", 22, 2]]],
    "1_3": [, ],
    "2_1": [[["b", 0]], [["b", 0]]],
}